'use strict';

var gulp = require('gulp'),
less     = require('gulp-less'),
uglify   = require('gulp-uglify'),
//rename = require('gulp-rename'),
plumber  = require('gulp-plumber'),
cssmin   = require('gulp-cssmin');



var path = {
    dist: {
        css:   'dist/css',
        js:    'dist/js',
        img:   'dist/img',
        fonts: 'dist/fonts',
        data:  'dist/data',
        html:  'dist',
    },
    build: {
          css: 'src/css',
    },
    src: {
        html:  'src/*.html',
        js:    'src/js/*.js',
        css:   'src/css/*.css',
        less:  'src/less/*.less',
        img:   'src/img/**/*.*',
        fonts: 'src/fonts/**/*.*',
        data:  'src/data/**/*.*',
    },
    watch: {
        html:  'src/**/*.html',
        js:    'src/js/**/*.js',
        css:   'src/css/**/*.css',
        less:  'src/css/**/*.less',
        img:   'src/img/**/*.*',
    },
    //clean: './build'
};


// построение less
gulp.task('boots:build', function () {
    var l = less({});
    l.on('error',function(e){
        console.log(e);
        l.end();
    });
    return gulp.src('../test_bootstrap/bootstrap.less')
        .pipe(l)
		//.pipe(cssmin())
		//.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest(path.build.css));
});


// построение less
gulp.task('less:build', function () {
    //.pipe(plumber({errorHandler: function (error) {console.log('Error: ' + error.message);this.emit('end');}}))    
    var l = less({});
    l.on('error',function(e){
        console.log(e);
        l.end();
    });
    return gulp.src('src/less/abba.less')
        .pipe(plumber({errorHandler: function (error) {console.log('Error: ' + error.message);this.emit('end');}}))
        .pipe(l)
		//.pipe(cssmin())
		//.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest(path.build.css));
});

// копирование css
gulp.task('copycss:build', function () {
    return gulp.src(path.src.css)
		.pipe(cssmin())
		.pipe(gulp.dest(path.dist.css));
});

// копирование html
gulp.task('copyhtml:build', function () {
    return gulp.src(path.src.html)
		.pipe(gulp.dest(path.dist.html));
});

// копирование изображений
gulp.task('img:build', function () {
    return gulp.src(path.src.img)
		.pipe(gulp.dest(path.dist.img));
});

gulp.task('ang:build', function () {
    var u = uglify({});
    u.on('error',function(e){
        console.log(e);
        u.end();
    });
    return gulp.src('src/js/angular/**/*.*')
		//.pipe(uglify())    
		.pipe(u)
		.pipe(gulp.dest('dist/js/angular'));
});

// копирование данных
gulp.task('data:build', function () {
    return gulp.src(path.src.data)
		.pipe(gulp.dest(path.dist.data));
});

// копирование шрифтов
gulp.task('fonts:build', function () {
    return gulp.src(path.src.fonts)
		.pipe(gulp.dest(path.dist.fonts));
});


// минификация копирование js
gulp.task('js:build', function () {
    var u = uglify({});
    u.on('error',function(e){
        console.log(e);
        u.end();
    });
    return gulp.src(path.src.js)
		.pipe(u)    
		//.pipe(uglify())
		//.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest(path.dist.js));
});


// задания в build
gulp.task('build', [
    'less:build',
    'copycss:build',
    'copyhtml:build',
    'img:build',
    'ang:build',
    'data:build',
    'fonts:build',
    'js:build'    
]);

gulp.task('default', ['build']);

gulp.task('mywatch', function () {
    gulp.watch('src/less/*.less', ['less:build']);
    gulp.watch('src/js/*.js', ['js:build']);
    gulp.watch('../test_bootstrap/bootstrap.less', ['boots:build']);
    gulp.watch('src/js/*.js', ['js:build']);
    gulp.watch('src/js/angular/*.js', ['ang:build']);
});


//gulp.task('mincss', function(){
//  return gulp.src('main.css')
//    .pipe(minifyCss())
//    .pipe(gulp.dest('main'));
//});
//
//gulp.task('scripts', function(){
//  return gulp.src(paths.script)
//    .pipe(coffee())
//    .pipe(gulp.dest('js'));
//});
//
//  gulp.task('watcher',function(){
//    gulp.watch(paths.css, ['mincss']);
//    gulp.watch(paths.script, ['scripts']);
//});