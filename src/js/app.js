var app = angular.module('myApp', ['ngMockE2E','ngAnimate', 'ngSanitize', 'ui.bootstrap']);

app.controller('myAppCtrl', [ '$scope', '$http', function ($scope, $http) {

    $scope.deliveryItems = [];
    $scope.searchString = {};
    $scope.activeDeliveryNumber = '';

    // текущие значения
    $scope.deliveryType = 'Pickpoint';
    $scope.displayType  = 'asList';

    // значения атрибутов кнопок для типов доставки
    $scope.checkDelivery = {
        Delivery: false,
        Pickup: false,
        Pickpoint: true
    };
    
    // значения атрибутов кнопок для типов отображения
    $scope.checkDisplay = {
        asList: true,
        asMap: false
    };

    // запрос данных
    $http.get("http://google.com/data.json").success(function(data) {
        if (data.success) {
            $scope.deliveryItems = data.data;
            $scope.showData = true;
        }
    })
    .error(function(data){
        // nothing
    });

    // переключение типа доставки
    $scope.setActiveDeliveryNumber = function (val) {
        $scope.$apply(function(){
            $scope.activeDeliveryNumber = val;
        });
    };
    
    // приведение поля workTime к многострочному виду
    $scope.getWorkTime = function(ind){        
        if ($scope.deliveryItems.length==0)
            return '';
        var str = $scope.deliveryItems[ind].workTime;
        str = str.replace('сб.', '<br/>сб.');
        str = str.replace('вс.', '<br/>вс.');
        return str;
    };
}]); // myAppCtrl

// директива для отображения кнопки ВЫБРАТЬ/ВЫБРАН
app.directive('myButton', function() {
    return{
        template: 'Выбрать',
        link: function(scope, elem, attrs){
            elem.addClass('btn my-button');
            if (attrs.myNumber == scope.activeDeliveryNumber) {
                elem.html("Выбран");
                elem.addClass('active');
            }
            elem.on('click', function(e){
                if (scope.activeDeliveryNumber == attrs.myNumber)
                    scope.setActiveDeliveryNumber("");
                else
                    scope.setActiveDeliveryNumber(attrs.myNumber);
            });
            scope.$watch('activeDeliveryNumber', function(newValue, oldValue, scope){
                if (scope.activeDeliveryNumber == attrs.myNumber){
                    elem.html("&nbsp;Выбран&nbsp;");
                    elem.addClass('active');
                } else {
                    elem.html("Выбрать");
                    elem.removeClass('active');
                }
            });
        }
    };
}); // directive myButton




app.run(['$httpBackend' , function($httpBackend) {
    var data = {
  "success" : true,
  "data" : [
    {
      "number": "C1010",
      "addressName" : "ул. Якубовича, д. 8А",
      "address": "123456, Россия, Санкт-Петербург, ул. Якубовича, д. 8А",
      "subway" : "Новокузнецкая",
      "text" : "Повседневная практика показывает, что укрепление и развитие структуры требуют от нас анализа систем массового участия. Не следует, однако забывать, что дальнейшее развитие различных форм деятельности способствует подготовки и реализации позиций, занимаемых участниками в отношении поставленных задач.",
      "coordinates": {
        "x": 59.932582,
        "y": 30.298730
      },
      "workTime": "пн.-пт. 10:00 — 22:00 сб. 13:00 — 18:00 вс. выходной",
      "director": "Баликов Александр",
      "deliveryData" : {
        "type": "dpd",
        "displayValue": "пункт выдачи DPD",
        "price": {
          "sum" : "569",
          "currency" : "руб"
        },
        "period": "2 дней",
        "timeHold" : "14 дней"
      }
    },
    {
      "number": "C1011",
      "addressName" : "Лиговский проспект, д. 12",
      "address": "123456, Россия, Санкт-Петербург, Лиговский проспект, д.12",
      "subway" : "Маяковская",
      "text" : "Повседневная практика показывает, что укрепление и развитие структуры требуют от нас анализа систем массового участия.",
      "coordinates": {
        "x": 59.914581,
        "y": 30.349910
      },
      "workTime": "пн.-пт. 10:00 — 22:00 сб. 13:00 — 18:00 вс. выходной",
      "director": "Баликов Александр",
      "deliveryData" : {
        "type": "dostavista",
        "displayValue": "пункт выдачи Достависта",
        "price": {
          "sum" : "312",
          "currency" : "руб"
        },
        "period": "1 день",
        "timeHold" : "30 дней"
      }
    },
    {
      "number": "C1013",
      "addressName" : "ул. Ефимова, д. 1",
      "address": "123456, Россия, Санкт-Петербург, ул. Ефимова, д.1",
      "subway" : "Сенная Площадь",
      "text" : "",
      "coordinates": {
        "x": 59.925863,
        "y": 30.320660
      },
      "workTime": "пн.-пт. 10:00 — 22:00 сб. 13:00 — 18:00 вс. выходной",
      "director": "Баликов Александр",
      "deliveryData" : {
        "type": "sibhealth",
        "displayValue": "пункт выдачи «Сибирское Здоровье»",
        "price": {
          "sum" : "0",
          "currency" : "руб"
        },
        "period": "1 день",
        "timeHold" : "5 дней"
      }
    },
    {
      "number": "C1014",
      "addressName" : "ул. Большая Пушкарская 25а",
      "address": "123456, Россия, Санкт-Петербург, ул. Большая Пушкарская 25а",
      "subway" : "Чкаловская",
      "text" : "Повседневная практика показывает, что укрепление и развитие структуры требуют от нас анализа систем массового участия. Не следует, однако забывать, что дальнейшее развитие различных форм деятельности способствует подготовки и реализации позиций, занимаемых участниками в отношении поставленных задач.",
      "coordinates": {
        "x": 59.959618,
        "y": 30.305293
      },
      "workTime": "пн.-пт. 10:00 — 22:00 сб. 13:00 — 18:00 вс. выходной",
      "director": "Баликов Александр",
      "deliveryData" : {
        "type": "dpd",
        "displayValue": "пункт выдачи DPD",
        "price": {
          "sum" : "569",
          "currency" : "руб"
        },
        "period": "2 дней",
        "timeHold" : "20 дней"
      }
    }
  ],
  "message" : "ОК"
};
  
    
$httpBackend.whenGET('http://google.com/data.json').respond(200, data);
    
}]);